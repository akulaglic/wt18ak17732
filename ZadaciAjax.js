var ZadaciAjax = (function(){
    var greskaJSON = {greska:"Vec ste uputili zahtjev"}
    var prekidJSON = {greska:"Prekinut zahtjev"}
    function myTimeoutFunction(ajax)
            {
                ajax.abort();
            }
            
    var konstruktor = function(callbackFn){
        var zahtjevUpucen = false;
    return {
    dajXML:function(){
        if (!zahtjevUpucen) {
            var ajax = new XMLHttpRequest();

            ajax.onreadystatechange = function() {// Anonimna funkcija
                if (ajax.readyState == 4 && ajax.status == 200) {
                    clearTimeout(timerId);
                    callbackFn(ajax.response);
                    zahtjevUpucen = false;
                }
                
            }
            
            ajax.open("GET", "http://localhost:8080/zadaci", true);
            ajax.setRequestHeader("Accept", "application/xml");
            ajax.setRequestHeader("Accept", "text/xml");
            ajax.send();
            var timerId = setTimeout(function(){callbackFn(prekidJSON);ajax.abort(); zahtjevUpucen = false;}, 2000);
            zahtjevUpucen = true;
            
        } else {
            callbackFn(greskaJSON)
            clearTimeout(timerId);
        }
    
    },
    dajCSV:function(){
        if (!zahtjevUpucen) {
            var ajax = new XMLHttpRequest();
            
            ajax.onreadystatechange = function() {// Anonimna funkcija
                if (ajax.readyState == 4 && ajax.status == 200) {
                    clearTimeout(timerId);
                    callbackFn(ajax.response);
                    zahtjevUpucen = false;
                }
            }
                
            ajax.open("GET", "http://localhost:8080/zadaci", true);
            ajax.setRequestHeader("Accept", "text/csv");
            ajax.send();
            var timerId = setTimeout(function(){callbackFn(prekidJSON);ajax.abort(); zahtjevUpucen = false;}, 2000);
            zahtjevUpucen = true;
           
            
        } else {
            callbackFn(greskaJSON)
            clearTimeout(timerId);
        }
    
    },
    dajJSON:function(){
        if (!zahtjevUpucen) {
            var ajax = new XMLHttpRequest();
            
            ajax.onreadystatechange = function() {// Anonimna funkcija
                if (ajax.readyState == 4 && ajax.status == 200) {
                    clearTimeout(timerId);
                    callbackFn(ajax.response);
                    zahtjevUpucen = false;
                }
                
            }
            ajax.open("GET", "http://localhost:8080/zadaci", true);
            ajax.setRequestHeader("Accept", "application/json");
            ajax.send();
            var timerId = setTimeout(function(){callbackFn(prekidJSON);ajax.abort(); zahtjevUpucen = false;}, 2000);
            zahtjevUpucen = true;
           
        } else {
            callbackFn(greskaJSON)
            clearTimeout(timerId);
        }
    
    }
    }
    }
    return konstruktor;
}());


    
var CommitTabela=(function(){

   
    
    var konstruktor=function(divElement,brojZadataka){
       //Kreiranje tabele
        var tabela = document.createElement('table');
    
        var tbdy = document.createElement('tbody');
        for (var i = 0; i < brojZadataka+1; i++) {
            var tr = document.createElement('tr');
            for (var j = 0; j < 2; j++) {
           
                var td = document.createElement('td');
               
                if (i==0) {
                    if (j==0) td.appendChild(document.createTextNode('Novi zadatak'))
                    if (j==1) td.appendChild(document.createTextNode('Commiti'))
                    
                }
                else if(i>0 && j==0) {
                    td.innerHTML = "<a href=" + "https://c2.etf.unsa.ba/login/index.php" +">"+"Zadatak " + i +"</a>";
                } 

                tr.appendChild(td)
            }
        
            tbdy.appendChild(tr);
        }
        tabela.appendChild(tbdy);
        divElement.appendChild(tabela);

        function MaximalnoKolona() {

            var max=0;
            for(var i=1; i<tabela.rows.length; i++) {
                if(max<tabela.rows[i].cells.length) max=tabela.rows[i].cells.length-1;
            }
            return max;
        }

        function MaximalniKomit(broj) {
            var max=0;
           
            for(var i=1; i<tabela.rows[broj].cells.length; i++) {
                var n=tabela.rows[broj].cells[i].innerText;
                if(max<Number(n)) max=Number(n)
            }
            return max
        }

     
        //Funkcija siri colspan svakom zadatku koji ima manje od maximuma komita

        function Sirenje() {
            for(var i=1; i<tabela.rows.length; i++) {
                var brojKolona = tabela.rows[i].cells.length;
                /*if(tabela.rows[i].cells.length < MaximalnoKolona() && tabela.rows[i].cells[brojKolona-1].innerHTML!="")  {
                    tabela.rows[i].insertCell(brojKolona);
                    tabela.rows[i].cells[brojKolona].colSpan = MaximalnoKolona();
                }*/
                if((brojKolona-1) < MaximalnoKolona() ) {
                    tabela.rows[i].cells[brojKolona-1] = MaximalnoKolona() - (brojKolona-1);

                }
                else if((brojKolona-1) == MaximalnoKolona()) {
                    tabela.rows[i].cells[brojKolona-1].colSpan = 1;
                }
                if(tabela.rows[i].cells[1].innerHTML == "") {
                    tabela.rows[i].cells[1].colSpan = MaximalnoKolona()
                } 
                else {
                    tabela.rows[i].cells[1].colSpan = 1
                }
            }
        }

       
        return{
        dodajCommit:function(rbZadatka,url){

            var brojKolona = tabela.rows[rbZadatka].cells.length;

            if(tabela.rows[rbZadatka].cells[brojKolona-1].innerHTML=="") {
                var novaCelija = tabela.rows[rbZadatka].cells[brojKolona-1]
               
                novaCelija.innerHTML= "<a href=" + url +">"+(brojKolona-1)+"</a>"
              
                Sirenje();
                
            }
            
            else {
                
                novaCelija = tabela.rows[rbZadatka].insertCell(brojKolona);
                var commitNaziv = tabela.rows[0].cells[1]
                commitNaziv.colSpan = MaximalnoKolona()+1;

                novaCelija.innerHTML= "<a href=" + url +">"+(MaximalniKomit(rbZadatka)+1)+"</a>"
              
                Sirenje();
               
            }
          
        },
        editujCommit:function(rbZadatka,rbCommita,url){

            if(rbZadatka<0 || rbZadatka>tabela.rows.length || rbCommita<0 || rbCommita>tabela.rows[rbZadatka].cells.length) return -1;
            else {
                tabela.rows[rbZadatka].cells[rbCommita].innerHTML= "<a href=" + url +">"+rbCommita+"</a>"
                return 0;
            } 
           
        
        },
        obrisiCommit:function(rbZadatka,rbCommita){
            if(rbZadatka<0 || rbZadatka>tabela.rows.length || rbCommita<0 || rbCommita>tabela.rows[rbZadatka].cells.length) return -1;
            else {
                tabela.rows[rbZadatka].deleteCell(rbCommita);
                return 0;
            } 
        }

        }
    }
    return konstruktor;
    }());

   

    
    


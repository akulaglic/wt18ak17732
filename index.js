const express = require('express');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser');
var path = require('path')
var URL= require('url');
var multer = require('multer');
const directoryPath = path.join(__dirname, '');
var jsonxml = require('json2xml');

//const Json2csvParser = require('json2csv').Parser;

var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, '');
    },
    filename: function(req, file, callback) {
       // console.log(req.file)
       //if(req.file == 'undefined') callback(null, false);
        let tijelo = req.body;

        let imePolja = tijelo['naziv'];
        callback(null, imePolja+".pdf");
       
    }
})
    
var upload = multer({storage: storage, fileFilter: function(req, file, callback){
    if(req.body["naziv"]=="") {
        return callback(null, false);
    }
    return callback(null, true);
}}, {dest:'./uploads/'});
//var upload = multer({dest:'./uploads/'});
app.use(express.static(path.join(__dirname, '')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//prvi zadatak
app.get('',function(request,response){
   // console.log('./'+request.url);
    switch(request.method) {
        case 'GET':
        
        fs.readFile('./'+request.url, 'utf-8', function (err, html) {
            
            if (err) {
                throw err; 
            } 
            response.writeHead(200, {"Content-Type": "text/html"});  
            response.write(html);  
            response.end();
        });
        break
    }
});

//drugi zadatak
app.post("/addZadatak",upload.single('postavka'), function(req, res, next){
    let tijelo = req.body;
    let imePolja = tijelo['naziv'];
    //console.log(util.inspect(req.file));
   
    if( imePolja=="" || req.file==null ) {
       
        fs.readFile('./greska.html', 'utf-8', function (err, html) {
            if (err) {
                throw err; 
            } 
            res.writeHead(200, {"Content-Type": "text/html"});  
            if(req.file==null) res.write("<h3>Niste unijeli sva polja!</h3>");
            else if(imePolja=="" ) res.write("<h3>Niste unijeli sva polja!</h3>");
           
            res.write(html);  
            res.end();
        });
    }
    else  {
		fs.exists(imePolja+'Zad.json', function(exists) {
           
                if(exists  || path.extname(req.file.originalname) != '.pdf') {
                    fs.readFile('./greska.html', 'utf-8', function (err, html) {
                        if (err) {
                            throw err; 
                        } 
                        res.writeHead(200, {"Content-Type": "text/html"});  
                        if(path.extname(req.file.originalname) != '.pdf') res.write("<h3>Ovo nije pdf file!</h3>");
                        else res.write("<h3> Postoji zadatak sa istim imenom!</h3>");
                        
                        res.write(html);  
                        res.end();
                    });


                } 
            else {
                //kreiranje JSON file-a
                var json = JSON.stringify({naziv:imePolja,postavka:"localhost:8080/"+imePolja+".pdf"});
                    fs.writeFile(imePolja+'Zad.json', json, function(err, content) {
                        if(err) throw err;

                        res.redirect('./addZadatak.html');
                    });
                }
            
        });
    
    }
});

//treci zadatak
app.get('/zadatak',function(req,response){
    
    var url1 = URL.parse(req.url);
    var parametar = new URL.URLSearchParams(url1.query);
    var naziv=parametar.get("naziv").toString();
    
    fs.readFile(naziv+'.pdf', function (err, content) {
        if (err) {
            throw err; 
        } 
        response.contentType("application/pdf");
        response.send(content);
    });

});

//cetvrti zadatak
app.post('/addGodina',function(req,response){

    fs.exists("godine.csv", function(exists) {         
        if (!exists) {
        
            fs.writeFile("godine.csv","godina,nazivRepVje,nazivRepSpi", function(err, content) {
                if (err) throw err;
                //funkcije su na kraju
                Zadatak4(response,req);
            });
        }
        else {
            // na kraju
            Zadatak4(response,req);
        }
    });
   
    
});
//peti zadatak
app.get('/godine',function(req,response){
   
    fs.exists("godine.csv", function(exists) {         
        if (!exists) {

            fs.writeFile("godine.csv", "godina,nazivRepVje,nazivRepSpi",function(err, content) {
                if (err) throw err;
                //na kraju
                Zadatak5(response);
            });
        } else {
            //na kraju
            Zadatak5(response);
        }
    });
    
    

});
//sedmi zadatak
app.get('/zadaci',function(req,res){
    var responseType;
    var acceptTypes = req.headers["accept"].split(", ");
  
    if(acceptTypes.includes('application/json') )  {
        
        res.setHeader('Content-Type', 'application/json')
        responseType = "JSON"
    }
    else if(acceptTypes.includes('application/xml'))  {
       
        res.setHeader('Content-Type', 'application/xml')
        responseType = "XML"
    }
    else if(acceptTypes.includes('text/xml'))  {
       
        res.setHeader('Content-Type', 'text/xml')
        responseType = "XML"
    }
    else if(acceptTypes.includes('text/csv'))  {
        
        res.setHeader('Content-Type', 'text/csv')
        responseType = "CSV"
    }
    else {
         
        res.setHeader('Content-Type', 'application/json')
        responseType = "JSON"
    }
    
    fs.readdir(directoryPath, function (err, files) {
        //handling error
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        } 
        var niz=[];
        var string="";
        var objekat=[];
        var i=0;
        var brojac=0;
            var j=0;
            for(var i=0; i<files.length; i++) {
                if(files[i].indexOf("Zad.json")>-1) {
                    j++;
                }
            }
        //listing all files using forEach
        files.forEach(function (file) {
           if(path.extname(file).toLowerCase() == '.json' && file!="package-lock.json" && file!="package.json") {
            //let rawdata = fs.readFileSync(file);  
            
            fs.readFile(__dirname+"/"+file, function (err, rawdata) {
                if (err) throw err;

                let json = JSON.parse(rawdata); 
                var naziv_zadatka = json.naziv;
                var postavka_zadatka = json.postavka;
                objekat[i] = {
                    naziv_zadatka, postavka_zadatka
                };
                niz.push(objekat[i]);
                string+=objekat[i].naziv_zadatka+","+objekat[i].postavka_zadatka+"\n";
                i=i+1;

                brojac++;
                
                if(brojac==j) {

                    switch (responseType) {
           
                        case "JSON":
                            res.end(JSON.stringify(niz))
                            break;
                        case "XML":
                            var noviJson = [];
                            for(var i=0; i<niz.length; i++) {
                                noviJson.push({zadatak : {naziv: niz[i].naziv_zadatka, postavka: niz[i].postavka_zadatka}})
                            }
                            res.end(jsonxml({ 'zadaci': noviJson}, { header: true }));
                            break;
                        case "CSV":
                           // const json2csvParser = new Json2csvParser({ fields, quote: '' });
                           // const csv = json2csvParser.parse(objekat);
                            res.end(string)
                            break;
                    }
                }


            });

            
        }
        });
       
    });
    
});
function Zadatak5(response) {
    fs.readFile("godine.csv", function(err, content){
        if (err) {
            throw err; 
        } 
        var niz=[];
        var text=content.toString();
        var redovi = text.split("\n");
    
        var objekat=[];
        for(var i=1; i<redovi.length; i++) {
            if (redovi[i].length != 0 ) {
                var kolone = redovi[i].split(",");
                
                    objekat[i] = {
                        nazivGod:kolone[0], nazivRepVje:kolone[1], nazivRepSpi:kolone[2]
                    };
                   
                    niz.push(objekat[i]);
            
            }
        }
        response.writeHead(200, {'Content-Type': "application/json"});
        
        response.end(JSON.stringify(niz));
       
       
    });
}

function Zadatak4 (response,req) {
    fs.readFile("godine.csv", function(err, content){
        if (err) {
           throw err;
            //console.log("error");
        } 
        
        var text=content.toString();
        var redovi = text.split("\n");
        var naziv_godine = req.body.nazivGod;
        var postoji = false;

       // console.log(naziv_godine);
        for(var i=0; i<redovi.length; i++) {
            var kolone = redovi[i].split(",");
           
            if( kolone[0].indexOf(naziv_godine)>-1 ){
                postoji = true;
                break
            }
            else {
                postoji = false; 
            }
        }
        if (postoji || req.body.nazivGod=="" || req.body.nazivRepSpi=="" || req.body.nazivRepVje=="") {
            fs.readFile('./greska.html', 'utf-8', function (err, html) {
                if (err) {
                    throw err; 
                } 
                response.writeHead(200, {"Content-Type": "text/html"});  
                if (req.body.nazivGod=="" && req.body.nazivRepSpi=="" && req.body.nazivRepVje=="")response.write("<h3>Niste unijeli sva polja!</h3>");
                else if(req.body.nazivGod==""|| req.body.nazivRepSpi==""|| req.body.nazivRepVje=="")response.write("<h3>Niste unijeli sva polja!</h3>");
                else response.write("<h3> Postoji godina sa istim imenom!</h3>");
                
                response.write(html);  
                response.end();
            });
        } else {
            let tijelo = req.body;
            let novaLinija = "\n"+tijelo['nazivGod']+","+tijelo['nazivRepVje']+","+tijelo['nazivRepSpi'];

            fs.appendFile('godine.csv',novaLinija,function(err){
                if(err) throw err;
               /* fs.readFile('./addGodina.html', 'utf-8', function (err, html) {
                    if (err) {
                        throw err; 
                    } 
                    response.writeHead(200, {"Content-Type": "text/html"});  
                    response.write(html);  
                    response.end();
                });*/
                response.redirect('./addGodina.html');
            });
           
        }
       
    });
}
app.listen(8080);



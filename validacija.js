var Validacija=(function(){
   
    

    var konstruktor=function(divElementPoruke){
        divElementPoruke.innerHTML = ""
        var listaGresaka = new Array();

        
        function IspisiPoruku(){
           divElementPoruke.innerHTML = "Sljedeca polja nisu validna:";
            if (listaGresaka.length == 0) {
                divElementPoruke.innerHTML = ""
            }
            else  divElementPoruke.innerHTML += listaGresaka.join(",")+"!";    
        }
        
       
       
        return{
        ime:function(inputElement){
            var regex =  /^([A-Z][a-z]*(\')?[a-z]+(\')?[a-z]*[-\s]?){0,3}([A-Z][a-z]*(\')?[a-z]+(\')?[a-z]*){1}$/;
          
            if (!regex.test(inputElement.value)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("ime"))listaGresaka.push("ime"); 
                IspisiPoruku()

            }
            else {
                inputElement.style.backgroundColor = 'white';
               
            }
             
        },
        
        
        godina:function(inputElement){
            
           // if(inputElement)
            var C=inputElement.value.charAt(2);
            var D=inputElement.value.charAt(3);

            var broj = Number(C+D)+1

             var regex = new RegExp("^(20)[0-9]{2}\/(20)"+broj+"$");
            if (!regex.test(inputElement.value)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("godina"))listaGresaka.push("godina"); 
                IspisiPoruku();
            }
            else inputElement.style.backgroundColor = 'white';
              
            

        },
        repozitorij:function(inputElement,regex){
           
            if (!regex.test(inputElement.value)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("repozitorij"))listaGresaka.push("repozitorij"); 
                IspisiPoruku()
               
            }
            else {
                inputElement.style.backgroundColor = 'white';
                
            }
            
        },
        index:function(inputElement){
            var regex = /^([0-1][4-9]|2[0])\d{3}$/;
           
            if (!regex.test(inputElement.value)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("index"))listaGresaka.push("index"); 
                IspisiPoruku()
                
            }
            else {
                inputElement.style.backgroundColor = 'white';
                
            }
            

        },
        naziv:function(inputElement){
            var regex = /^[A-Za-z](\w|\\|\/|\-|“|‘|'|"|\!|\?|:|;|,)+(\d|[a-z])+$/;
           
            if (!regex.test(inputElement.value)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("naziv"))listaGresaka.push("naziv"); 
                IspisiPoruku()
               
            }
            else inputElement.style.backgroundColor = 'white';
           

        },
        password:function(inputElement){
            var regex = /^((?=(.*[a-z]){2,})|(?=(.*[A-Z]){2,})|(?=(.*[0-9]){2,})).{8,}$/;
            var specialregex = /\W/

             if (!inputElement.value.match(regex) || inputElement.value.match(specialregex)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("password"))listaGresaka.push("password"); 
                IspisiPoruku()
               
             }
             else{
                inputElement.style.backgroundColor = 'white';
               
             } 

            
                

        },
        url:function(inputElement){
            var regex = /^((http)|(https)|(ftp)|(ssh)):\/\/(([a-z0-9]+\.+)*[a-z0-9]+)((([a-z0-9]+-*[a-z0-9]+)\/+)*([a-z0-9]+-*[a-z0-9]+))*(\?([a-z0-9]+-*[a-z0-9]+)=([a-z0-9]+-*[a-z0-9]+)&([a-z0-9]+-*[a-z0-9]+)=([a-z0-9]+-*[a-z0-9]+))*$/;
            //var regex1=/^((([a-z0-9]+-*[a-z0-9]+)\/+)*([a-z0-9]+-*[a-z0-9]+))$/
           // var regex2= /\?([a-z0-9]+-*[a-z0-9]+)=([a-z0-9]+-*[a-z0-9]+)&([a-z0-9]+-*[a-z0-9]+)=([a-z0-9]+-*[a-z0-9]+)$/
           //var reg = /^(\w+/s+)*(\w+)/
            if ( !regex.test(inputElement.value)) {
                inputElement.style.backgroundColor = 'orangered';
                if (!listaGresaka.includes("url"))listaGresaka.push("url"); 
                IspisiPoruku()
            
            }
            else inputElement.style.backgroundColor = 'white';
            

        }
        }
    }

    return konstruktor;
}());

